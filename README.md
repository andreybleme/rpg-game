**1. Requirements**:

- JDK 8
- MySQL database


**2. Create Database**

Open you command line interface and access your mysql as a root user:

`mysql -u root -p`

Then paste the below command in you terminal:


```
create database rpggame;
create user 'rpggame'@'localhost' identified by 'rpggame';
grant all privileges on rpggame . * to 'rpggame'@'localhost';
flush privileges;
```


**3. Generate JAR file**

Go to the project root folder (where the pom.xml is) and execute the following commad:

`mvn package -Dmaven.test.skip=true`

A new file named rpg-game-0.0.1-SNAPSHOT.jar should be created at `rpg-game/target`.


**4. Running the Game**

Go to the `rpg-game/target` and execute:

`java -jar rpg-game-0.0.1-SNAPSHOT.jar`


The game will start.

ps: Make sure the rpg-game-0.0.1-SNAPSHOT.jar file was successfully generated on **step 3**.


**5. Running tests**

To run the Unit Tests, open the `/rpg-game/src/main/resources/application.properties` file.

Change the `dev` profile to `test`.

You file should look like this:

`spring.profiles.active=test`


Go to the project root folder (where the pom.xml is) and execute the following commad:

`mvn test`

You should see a message like this at the end:


```
Results :

Tests run: 24, Failures: 0, Errors: 0, Skipped: 0
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 7.290 s
[INFO] Finished at: 2018-09-13T00:04:11-03:00
[INFO] Final Memory: 19M/264M
[INFO] ------------------------------------------------------------------------

```