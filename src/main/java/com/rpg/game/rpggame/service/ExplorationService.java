package com.rpg.game.rpggame.service;

import java.util.List;

import com.rpg.game.rpggame.domain.Exploration;

public interface ExplorationService {

  Exploration getRandomDiscoveryExploration();
  
  List<Exploration> getAll();
}
