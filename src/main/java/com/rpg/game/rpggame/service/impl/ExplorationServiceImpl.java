package com.rpg.game.rpggame.service.impl;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rpg.game.rpggame.domain.Exploration;
import com.rpg.game.rpggame.factory.ExplorationFactory;
import com.rpg.game.rpggame.service.ExplorationService;

@Component
public class ExplorationServiceImpl implements ExplorationService {
  
  @Autowired
  private ExplorationFactory explorationFactory;

  @Override
  public Exploration getRandomDiscoveryExploration() {
    List<Exploration> explorations = explorationFactory.createGameExplorations();
    Random rand = new Random();
    return explorations.get(rand.nextInt(explorations.size() - 1));
  }

  @Override
  public List<Exploration> getAll() {
    return explorationFactory.createGameExplorations();
  }

}
