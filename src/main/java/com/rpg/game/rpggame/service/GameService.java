package com.rpg.game.rpggame.service;

import java.util.List;

import com.rpg.game.rpggame.domain.Game;
import com.rpg.game.rpggame.exception.GameServiceException;
import com.rpg.game.rpggame.domain.Character;

public interface GameService {
  
  Game createNewGame(Character character);
  
  Game saveGame(Game game);

  Game resumeGame(Long id) throws GameServiceException;
  
  List<Game> getAllSavedGames() throws GameServiceException;
}
