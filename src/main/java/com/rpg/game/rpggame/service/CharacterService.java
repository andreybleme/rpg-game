package com.rpg.game.rpggame.service;

import com.rpg.game.rpggame.domain.Character;
import com.rpg.game.rpggame.domain.Exploration;
import com.rpg.game.rpggame.exception.CharacterServiceException;

public interface CharacterService {
  
  Character create(Character character) throws CharacterServiceException;
  
  Character fight(Character character);
  
  Exploration explore(Character character);

}
