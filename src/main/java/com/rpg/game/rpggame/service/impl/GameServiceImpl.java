package com.rpg.game.rpggame.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rpg.game.rpggame.domain.Character;
import com.rpg.game.rpggame.domain.Game;
import com.rpg.game.rpggame.exception.GameServiceException;
import com.rpg.game.rpggame.repository.GameRepository;
import com.rpg.game.rpggame.service.CharacterService;
import com.rpg.game.rpggame.service.GameService;

@Component
public class GameServiceImpl implements GameService {
  
  @Autowired
  private CharacterService characterService;
  
  @Autowired
  private GameRepository gameRepository;
  
  @Override
  public Game createNewGame(Character character) {
    characterService.create(character);
    Game game = new Game(character);
    return gameRepository.save(game);
  }
  
  @Override
  public Game saveGame(Game game) {
    if (game == null) {
      throw new GameServiceException("Game cannot be null");
    }
    game.setLastSavedAt(new Date());
    return gameRepository.save(game);
  }
  
  @Override
  public Game resumeGame(Long id) {
    Game game = gameRepository.findOne(id);
    if (game == null) {
      throw new GameServiceException("Game not found with ID: " + id);
    }
    return game;
  }

  @Override
  public List<Game> getAllSavedGames() {
    List<Game> games = gameRepository.findAll();
    if (games.isEmpty()) {
      throw new GameServiceException("No saved games were found");
    }
    return games;
  }

}
