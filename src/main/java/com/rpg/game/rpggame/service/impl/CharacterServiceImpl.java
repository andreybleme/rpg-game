package com.rpg.game.rpggame.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rpg.game.rpggame.domain.Character;
import com.rpg.game.rpggame.domain.Exploration;
import com.rpg.game.rpggame.exception.CharacterServiceException;
import com.rpg.game.rpggame.repository.CharacterRepository;
import com.rpg.game.rpggame.service.CharacterService;
import com.rpg.game.rpggame.service.ExplorationService;

@Component
public class CharacterServiceImpl implements CharacterService {

  @Autowired
  private CharacterRepository characterRepository;
  
  @Autowired
  private ExplorationService explorationService;
  
  private static final String ANSI_RESET = "\u001B[0m";
  private static final String ANSI_PURPLE = "\u001B[35m";
  
  @Override
  public Character create(Character character) {
    if (character == null) {
      throw new CharacterServiceException("Character cannot be null");
    }
    if (character.getName() == null 
        || character.getName().isEmpty()) {
      throw new CharacterServiceException("Character name cannot be empty");
    }
    return characterRepository.save(character);
  }

  @Override
  public Character fight(Character character) {
    Integer earnedExperience = (int) (Math.random() * 10 + 1);
    character.setExperience(earnedExperience);
    System.out.print(ANSI_PURPLE + "You won a fight! You earned +" + earnedExperience + " XP and some scars..." + ANSI_RESET);
    
    return characterRepository.save(character);
  }

  @Override
  public Exploration explore(Character character) {
    Exploration exploration = explorationService.getRandomDiscoveryExploration();
    System.out.print(ANSI_PURPLE + "\n" + character.getName() + ", " + exploration.getMessage() + ANSI_RESET);
    
    return exploration;
  }
  
}
