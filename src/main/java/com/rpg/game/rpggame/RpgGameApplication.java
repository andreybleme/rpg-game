package com.rpg.game.rpggame;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

import com.rpg.game.rpggame.client.RpgGameClient;

@SpringBootApplication
public class RpgGameApplication {
  
  @Autowired
  private RpgGameClient rpgGameClient;
  
  @Autowired
  private Environment environment;

  @PostConstruct
  public void postConstruct() {
    if (!environment.getActiveProfiles()[0].equals("test")) {
      rpgGameClient.runGame(); 
    } 
  }

  public static void main(String[] args) {
    SpringApplication.run(RpgGameApplication.class, args);
  }
  
}
