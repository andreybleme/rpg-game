package com.rpg.game.rpggame.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rpg.game.rpggame.domain.Game;

public interface GameRepository extends JpaRepository<Game, Long> {

}
