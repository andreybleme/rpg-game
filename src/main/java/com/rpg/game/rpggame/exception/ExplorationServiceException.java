package com.rpg.game.rpggame.exception;

public class ExplorationServiceException extends RuntimeException {
  
  private static final long serialVersionUID = -847583971047106123L;

  public ExplorationServiceException(String message) {
    super(message);
  }

  public ExplorationServiceException(String message, Throwable t) {
    super(message, t);
  } 

}
