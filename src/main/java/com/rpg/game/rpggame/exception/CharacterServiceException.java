package com.rpg.game.rpggame.exception;

public class CharacterServiceException extends RuntimeException {

  private static final long serialVersionUID = 1617112273360951854L;

  public CharacterServiceException(String message) {
    super(message);
  }   

  public CharacterServiceException(String message, Throwable t) {
      super(message, t);
  }

}
