package com.rpg.game.rpggame.exception;

public class GameServiceException extends RuntimeException {
  
  private static final long serialVersionUID = 2446187459614498655L;

  public GameServiceException(String message) {
    super(message);
  }

  public GameServiceException(String message, Throwable t) {
    super(message, t);
  } 

}
