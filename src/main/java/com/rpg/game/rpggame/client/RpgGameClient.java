package com.rpg.game.rpggame.client;

import java.util.List;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rpg.game.rpggame.domain.Character;
import com.rpg.game.rpggame.domain.Game;
import com.rpg.game.rpggame.exception.CharacterServiceException;
import com.rpg.game.rpggame.exception.GameServiceException;
import com.rpg.game.rpggame.service.CharacterService;
import com.rpg.game.rpggame.service.GameService;

@Component
public class RpgGameClient {
  
  @Autowired
  private GameService gameService;
  
  @Autowired
  private CharacterService characterService;
  
  private static Scanner in = new Scanner(System.in);
  
  private static final String ANSI_RESET = "\u001B[0m";
  private static final String ANSI_RED = "\u001B[31m";
  private static final String ANSI_GREEN = "\u001B[32m";
  private static final String ANSI_BLUE = "\u001B[34m";
  private static final String ANSI_CYAN = "\u001B[36m";
  
  private static final int NEW_GAME = 1;
  private static final int LOAD_GAME = 2;
  private static final int EXIT_GAME = 9;
  

  public void runGame() {
    System.out.print(ANSI_CYAN + " WELCOME TO RPG GAME " + ANSI_RESET);
    
    menuLoop: while (true) {
      System.out.print("\n\nType the number of an option below to continue: ");
      System.out.print(ANSI_BLUE + "\n 1" + ANSI_RESET +" New Game");
      System.out.print(ANSI_BLUE + "\n 2" + ANSI_RESET +" Load Game");
      System.out.print(ANSI_BLUE + "\n 9" + ANSI_RESET +" Exit");
      System.out.print(ANSI_BLUE + "\n\n > " + ANSI_RESET);
      
      Integer option = in.nextInt();

      switch (option) {
        case NEW_GAME:
          System.out.print("\nType here the name of your new Character");
          System.out.print(ANSI_BLUE + "\n\n > " + ANSI_RESET);
          
          in.nextLine();
          String name = in.nextLine();
          
          try {
            Character character = new Character(name);
            Game game = gameService.createNewGame(character);
            startGame(game);
          } catch (CharacterServiceException e) {
            System.out.print(ANSI_RED + "\n" + e.getMessage() + ANSI_RESET);
          }
          break;
          
        case LOAD_GAME:
          System.out.print("\nType the game ID you want to resume (example: 2)");
          
          try {
            List<Game> savedGames = gameService.getAllSavedGames();  
            savedGames.forEach(g -> {
              System.out.println("\n" + ANSI_BLUE + g.getId() + ANSI_RESET + ". " 
                  + g.getCharacter().getName() + "(" + g.getCharacter().getExperience() + "XP), "
                  + "created at: " + g.getCreatedAt().toString().replaceAll("T", ""));
            });
            
            System.out.print(ANSI_BLUE + "\n\n > " + ANSI_RESET);
            Integer optionGameID = in.nextInt();
            
            Game gameToResume = gameService.resumeGame(optionGameID.longValue());
            startGame(gameToResume);
          } catch (GameServiceException e) {
            System.out.print(ANSI_RED + "\n" + e.getMessage() + ANSI_RESET);
          }
          break;
        
        case EXIT_GAME:
          break menuLoop;

        default:
          System.out.print(ANSI_RED + "\nYou typed an invalid option." + ANSI_RESET);
          break;
      }  
    }
  }
  
  private void startGame(Game game) {
    System.out.print(ANSI_CYAN + "\n\nWelcome " + game.getCharacter().getName() + "!" + ANSI_RESET);
    System.out.print(ANSI_CYAN + "\n\nYou have " + game.getCharacter().getExperience() + "XP so far. Let's get some more!!!" + ANSI_RESET);
    
    final int EXPLORE = 1;
    final int FIGHT = 2;
    final int SAVE = 3;
    final int QUIT = 9;
    
    gameLoop: while(true) {
      System.out.print("\nType the number of an option above:");
      System.out.print(ANSI_BLUE + "\n 1" + ANSI_RESET +" Explore");
      System.out.print(ANSI_BLUE + "\n 2" + ANSI_RESET +" Fight");
      System.out.print(ANSI_BLUE + "\n 3" + ANSI_RESET +" Save Game");
      System.out.print(ANSI_RED + "\n 9" + ANSI_RESET +" Quit");
      System.out.print(ANSI_BLUE + "\n\n > " + ANSI_RESET);
      
      Integer option = in.nextInt();
      switch (option) {
        case EXPLORE:
          characterService.explore(game.getCharacter());
          break;

        case FIGHT:
          characterService.fight(game.getCharacter());
          break;
        
        case SAVE:
          gameService.saveGame(game);
          System.out.print(ANSI_GREEN + "\nGame saved" + ANSI_RESET);
          break;
        
        case QUIT:
          System.out.print(ANSI_GREEN + "\nGood bye!" + ANSI_RESET);
          break gameLoop;
          
        default:
          System.out.print(ANSI_RED + "\nYou typed an invalid option." + ANSI_RESET + "\n");
          continue;
      }
    }
  }

}
