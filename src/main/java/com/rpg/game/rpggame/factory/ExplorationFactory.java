package com.rpg.game.rpggame.factory;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.rpg.game.rpggame.domain.Exploration;

@Component
public class ExplorationFactory {
  
  private List<Exploration> explorations = new ArrayList<>();
  
  public List<Exploration> createGameExplorations() {
    generateBatmanExplorations();

    return this.explorations;
  }

  private void generateBatmanExplorations() {
    Exploration alfredExploration = new Exploration();
    alfredExploration.setCode("batman.alfred");
    alfredExploration.setMessage("Alfred is back from his vacation (finally)! He just fixed the Batmobile. You don't have to go out late night using the lamborghini anymore.");
    
    Exploration bankExploration = new Exploration();
    bankExploration.setCode("batman.thieves");
    bankExploration.setMessage("You defeated a gang of thieves in the Gotham City Bank! The mayor of Gotham gave you the city key at a gala event.");
    
    Exploration luciusExploration = new Exploration();
    luciusExploration.setCode("batman.lucius");
    luciusExploration.setMessage("Commissioner Gordon saw you talking to Lucius. He is wary of about who is behind the Batman mask. Be more discreet!");
    
    explorations.add(alfredExploration);
    explorations.add(bankExploration);
    explorations.add(luciusExploration);
  }

}
