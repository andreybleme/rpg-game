package com.rpg.game.rpggame.factory;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.rpg.game.rpggame.domain.Exploration;

@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest
public class ExplorationFactoryTest {
  
  @Autowired
  private ExplorationFactory explorationFactory;
  
  @Test
  public void createGameDiscoveryExplorations() {
    List<Exploration> explorations = explorationFactory.createGameExplorations();
    assertTrue("Explorations must be created by a Factory", explorations.size() > 1);
  }

}
