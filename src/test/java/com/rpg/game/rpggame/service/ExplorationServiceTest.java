package com.rpg.game.rpggame.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.rpg.game.rpggame.domain.Exploration;

@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest
public class ExplorationServiceTest {
  
  @Autowired
  private ExplorationService explorationService;
  
  @Rule
  public ExpectedException thrown = ExpectedException.none();
    
  @Test
  public void testSuccessGetRandomExploration() {
    Exploration randomExploration = explorationService.getRandomDiscoveryExploration();
    assertNotNull("A random Exploration must be found", randomExploration.getCode());
  }
  
  @Test
  public void testSuccessGetAllExplorations() {
    List<Exploration> allGameExplorations = explorationService.getAll();
    assertTrue(allGameExplorations.size() > 0);
  }


}
