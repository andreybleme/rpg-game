package com.rpg.game.rpggame.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.rpg.game.rpggame.domain.Character;
import com.rpg.game.rpggame.domain.Game;
import com.rpg.game.rpggame.exception.GameServiceException;
import com.rpg.game.rpggame.repository.GameRepository;

@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest
public class GameServiceTest {
  
  @Autowired
  private GameService gameService;
  
  @Autowired
  private GameRepository gameRepository;
  
  private static final Long INVALID_ID = 100L;
  private static final String EXCEPTION_NULL_MESSAGE = "Game cannot be null";
  private static final String EXCEPTION_RESUME_MESSAGE = "Game not found with ID: " + INVALID_ID;
  private static final String EXCEPTION_SAVEDGAMES_MESSAGE = "No saved games were found";
  
  @Rule
  public ExpectedException thrown = ExpectedException.none();
  
  @Test
  public void testSuccessCreateNewGame() {
    Character character = new Character("Batman");
    Game game = gameService.createNewGame(character);
    
    assertNotNull(game.getCreatedAt());
    assertEquals("New character should have 0 experience", game.getCharacter().getExperience(), 0);
  }
  
  @Test
  public void testSuccessSaveGame() {
    Character character = new Character("Batman");
    Game game = gameService.createNewGame(character);
    
    assertNull("New game show have a null Last Saved Date", game.getLastSavedAt());
    
    game.getCharacter().setExperience(100);
    gameService.saveGame(game);
    
    assertNotNull("Saved game must have a Last Saved Date", game.getLastSavedAt());
  }
  
  @Test
  public void testErrorSaveGameWhenGameIsNull() {
    thrown.expect(GameServiceException.class);
    thrown.expectMessage(EXCEPTION_NULL_MESSAGE);
    
    gameService.saveGame(null);
  }
  
  @Test
  public void testSuccessResumeGame() {
    Character character1 = new Character("Batman");
    Game game1 = gameService.createNewGame(character1);
    
    Character character2 = new Character("Superman");
    gameService.createNewGame(character2);
    
    Game resumedGame1 = gameService.resumeGame(game1.getId());
    assertEquals("Resumed game must correspond to its created game", resumedGame1, game1);
  }
  
  @Test
  public void testErrorResumeGameWhenIdDoesNotExists() {
    thrown.expect(GameServiceException.class);
    thrown.expectMessage(EXCEPTION_RESUME_MESSAGE);
    
    gameService.resumeGame(INVALID_ID);
  }
  
  @Test
  public void testSuccessGetAllSavedGames() {
    gameRepository.deleteAll();
    Character character1 = new Character("Flash");
    gameService.createNewGame(character1);
    
    Character character2 = new Character("Aquaman");
    gameService.createNewGame(character2);
    
    List<Game> games = gameService.getAllSavedGames();
    assertEquals("All existing games should be returned", games.size(), 2);
  }
  
  @Test
  public void testErrorGetAllSavedGamesWhenNoGamesExists() {
    thrown.expect(GameServiceException.class);
    thrown.expectMessage(EXCEPTION_SAVEDGAMES_MESSAGE);
    
    gameRepository.deleteAll();
    gameService.getAllSavedGames();
  }

}
