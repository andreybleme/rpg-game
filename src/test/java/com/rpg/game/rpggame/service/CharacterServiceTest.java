package com.rpg.game.rpggame.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.rpg.game.rpggame.domain.Character;
import com.rpg.game.rpggame.domain.Exploration;
import com.rpg.game.rpggame.exception.CharacterServiceException;

@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest
public class CharacterServiceTest {
  
  @Autowired
  private CharacterService characterService;
  
  @Rule
  public ExpectedException thrown = ExpectedException.none();
  
  private static final String BATMAN_NAME = "Batman";
  private static final String EXCEPTION_EMPTYNAME_MESSAGE = "Character name cannot be empty";
  private static final String EXCEPTION_NULL_MESSAGE = "Character cannot be null";
  
  @Test
  public void testSuccessCreateCharacter() {
    Character character = new Character(BATMAN_NAME);
    character = characterService.create(character);
  
    assertNotNull(character.getId());
  }
  
  @Test
  public void testErrorCreateCharacterWhenCharacterIsNull() throws CharacterServiceException {
    thrown.expect(CharacterServiceException.class);
    thrown.expectMessage(EXCEPTION_NULL_MESSAGE);
    
    characterService.create(null);
  }
  
  @Test
  public void testErrorCreateCharacterWhenNameIsEmpty() throws CharacterServiceException {
    thrown.expect(CharacterServiceException.class);
    thrown.expectMessage(EXCEPTION_EMPTYNAME_MESSAGE);
    
    Character character = new Character("");
    character = characterService.create(character);
  }
  
  @Test
  public void testErrorCreateCharacterWhenNameIsNull() throws CharacterServiceException {
    thrown.expect(CharacterServiceException.class);
    thrown.expectMessage(EXCEPTION_EMPTYNAME_MESSAGE);
    
    Character character = new Character();
    character = characterService.create(character);
  }
 
  @Test
  public void testSuccessWhenFighting() {
    Character character = new Character(BATMAN_NAME);
    assertEquals("A new character must have zero experience", character.getExperience(), 0);
    
    character = characterService.fight(character);
    assertTrue("After fighting, a character must have more that zero of experience", 
      character.getExperience() > 0);
  }
  
  @Test
  public void testSuccessWhenExploring() {
    Character character = new Character(BATMAN_NAME);
    Exploration exploration = characterService.explore(character);
    
    assertNotNull("A message must exist when a character explores", exploration.getMessage());
  }

}
